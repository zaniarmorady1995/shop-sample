import { Body, Controller, Delete, Get, Param, Patch, Post, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Roles } from 'src/general/role.decorator';
import { Role } from 'src/general/role.enum';
import { JwtAuthGuard } from 'src/guard/jwt.guard';
import { RolesGuard } from 'src/guard/role.guard';
import { AddLanguageDTO } from './dto/add-language.dto';
import { UpdateLanguageDTO } from './dto/update-language.dto';
import { Language } from './interface/language.interface';
import { LanguageService } from './language.service';

@UseGuards(JwtAuthGuard, RolesGuard)
@Roles(Role.Admin)
@ApiTags('Language')
@Controller('language')
export class LanguageController {

  constructor(private readonly _languageService: LanguageService) {}

  
  @Post()
  async add(@Body() dto: AddLanguageDTO): Promise<Language> {
    return this._languageService.add(dto);
  }

  @Get(':id')
  async findById(@Param('id') id: string): Promise<Language> {
    return this._languageService.findById(id);
  }

  @Delete(':id')
  async deleteById(@Param('id') id: string): Promise<Language> {
    return this._languageService.deleteById(id);
  }

  @Get()
  async findAll(): Promise<Language[]> {
    return this._languageService.findAll();
  }

  @Patch()
  async update(@Body() dto: UpdateLanguageDTO): Promise<Language> {
    return this._languageService.update(dto);
  }
  
}
