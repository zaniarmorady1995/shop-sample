import * as mongoose from "mongoose";
import { SchemaName } from "src/utils/constants";

export const LanguageSchema = new mongoose.Schema(
  {

    title: { // english
      type: String,
      trim: true,
      required: true,
      lowercase: true
    },

    code: { // en
      type: String,
      trim: true,
      required: true,
      lowercase: true
    },

    words: {
      type: mongoose.Schema.Types.Mixed
    }

  },

  { 
    timestamps: true,
    collection: SchemaName.Language,
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
      },
    },
  },
);

LanguageSchema.index({code: 1});