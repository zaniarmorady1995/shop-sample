import { InjectModel } from "@nestjs/mongoose";
import { BaseRepository } from "src/general/base.repository";
import { SchemaName } from "src/utils/constants";
import { Language } from "../interface/language.interface";
import { Model } from 'mongoose';
import { UpdateLanguageDTO } from "../dto/update-language.dto";
import { Injectable } from "@nestjs/common";

@Injectable()
export class LanguageRepository extends BaseRepository<Language> {

  constructor(
    @InjectModel(SchemaName.Language)
    private readonly _languageModel: Model<Language>
  ) {
    super(_languageModel)
  }


  async update(object: UpdateLanguageDTO): Promise<Language> {
    let updateObject = {
      title: object.title,
      code: object.code,
    };

    Object.entries(object.words).forEach(([key,value]) => {
      updateObject[`words.${key}`] = value;
    });
    
    return this._languageModel.findByIdAndUpdate(
      object.id,
      updateObject,
      { new: true }
    );
  }


  async findByCode(code: string): Promise<Language> {
    return this._languageModel.findOne({code})
  }

}