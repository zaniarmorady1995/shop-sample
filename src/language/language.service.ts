import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { LanguageRepository } from './database/language.repository';
import { AddLanguageDTO } from './dto/add-language.dto';
import { UpdateLanguageDTO } from './dto/update-language.dto';
import { Language } from './interface/language.interface';


@Injectable()
export class LanguageService {
  
  private _items: Language[] = [];

  constructor(
    private readonly _languageRepository: LanguageRepository
  ) {}


  async add(dto: AddLanguageDTO): Promise<Language> {
    try {
      await this.findByCode(dto.code);  
    } catch (_) {
      const newLanguage = await this._languageRepository.create(dto);
      this._items.push(newLanguage);
      return newLanguage;  
    }
  }


  async findById(id: string): Promise<Language> {
    const language = await this._languageRepository.findById(id)
    if (!language) throw new NotFoundException('Language not found');
    return language;
  }


  async findAll(): Promise<Language[]> {
    return this._languageRepository.findAll();
  }


  async update(dto: UpdateLanguageDTO): Promise<Language> {
    const language = await this._languageRepository.update(dto);
    const index = this._items.findIndex(e => e.id == language.id);
    this._items[index] = language;
    return language;
  }


  async deleteById(id: string): Promise<Language> {
    const language = await this._languageRepository.deleteById(id);
    if (!language) throw new NotFoundException('Language not found');
    this._items.forEach((element,index) => {
      if(element.id == id) this._items.splice(index, 1);
    });
    return language;
  }


  async findByCode(code: string): Promise<Language> {
    const language = await this._languageRepository.findByCode(code);
    if (!language) throw new NotFoundException('Language not found');
    return language;
  }


  async prepareKeys(code: string, keys: string[]): Promise<object> {
    let response = {};
    const language = await this.findLanguageByCode(code);

    Object.entries(language.words).forEach(([key,value]) => {
      if(keys.includes(key)) {
        response[key] = value;
        const index = keys.indexOf(key, 0);
        keys.splice(index, 1);
      }
    });

    keys.forEach(key => response[key] = key);
    return response;
  }


  private async findLanguageByCode(code: string): Promise<Language> {
    let index = this._items.findIndex(e => e.code == code);
    if (index != -1) return this._items[index];
    
    const language = await this.findByCode(code);
    this._items.push(language);
    return language;
  }

}
