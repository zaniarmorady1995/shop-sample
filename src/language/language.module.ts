import { Module } from '@nestjs/common';
import { LanguageService } from './language.service';
import { LanguageController } from './language.controller';
import { SchemaName } from 'src/utils/constants';
import { MongooseModule } from '@nestjs/mongoose';
import { LanguageSchema } from './database/language.schema';
import { LanguageRepository } from './database/language.repository';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: SchemaName.Language, schema: LanguageSchema}])
  ],
  controllers: [LanguageController],
  providers: [
    LanguageService,
    LanguageRepository,
  ],
  exports: [LanguageService]
})
export class LanguageModule {}
