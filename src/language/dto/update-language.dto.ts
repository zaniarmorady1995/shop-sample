import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsNotEmpty, IsOptional, ValidateNested } from "class-validator";

export class UpdateLanguageDTO {
  @ApiProperty()
  @IsNotEmpty()
  id: string;
  
  @ApiPropertyOptional({description: 'Such as english, persian'})
  @IsOptional()
  title: string;

  @ApiPropertyOptional({description: 'Such as en, fa'})
  @IsOptional()
  code: string;

  @ApiPropertyOptional({description: 'color: رنگ'})
  @IsOptional()
  words: object;
}