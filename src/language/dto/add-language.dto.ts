import { ApiProperty } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsNotEmpty, ValidateNested } from "class-validator";

export class AddLanguageDTO {
  @ApiProperty({description: 'Such as english, persian'})
  @IsNotEmpty()
  title: string;

  @ApiProperty({description: 'Such as en, fa'})
  @IsNotEmpty()
  code: string;

  @ApiProperty()
  @IsNotEmpty()
  words: object;
}