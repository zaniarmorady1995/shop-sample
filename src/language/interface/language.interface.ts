import { Document } from 'mongoose'; 

export interface Language extends Document {
  id: string;
  title: string;
  code: string;
  words: object;
}