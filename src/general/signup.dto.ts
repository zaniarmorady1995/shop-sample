import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsNotEmpty, IsOptional, IsString, MaxLength, MinLength,  } from "class-validator";
import { SigninDTO } from "../auth/dto/signin.dto";

export class SignupDTO extends SigninDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  @MaxLength(32)
  name: string;

  @ApiPropertyOptional()
  @IsOptional()
  avatar: string;

  @ApiProperty()
  @IsNotEmpty()
  languageCode: string;
}