import { Injectable } from "@nestjs/common";
import { Model } from 'mongoose';

@Injectable()
export class BaseRepository<T> {

  constructor(private readonly _model: Model<T>) {}

  async create(object: Partial<T>): Promise<any> {
    return new this._model(object).save();
  }

  async findAll(): Promise<T[]> {
    return this._model.find();
  }

  async findById(id: string): Promise<T> {
    return this._model.findById(id);
  }

  async deleteById(id: string): Promise<T> {
    return this._model.findByIdAndDelete(id);
  }
}