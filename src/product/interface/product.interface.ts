import { Document } from 'mongoose'; 

export interface Product extends Document {
  id: string;
  name: string;
  image_url: string;
  price: number;
  details: object;
}