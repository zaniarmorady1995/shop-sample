import { Body, Controller, Delete, Get, Param, Patch, Post, Query, UseGuards} from '@nestjs/common';
import { ApiParam, ApiTags } from '@nestjs/swagger';
import { Roles } from 'src/general/role.decorator';
import { Role } from 'src/general/role.enum';
import { JwtAuthGuard } from 'src/guard/jwt.guard';
import { RolesGuard } from 'src/guard/role.guard';
import { GetCurrentUserById } from 'src/utils/get-current-id';
import { CreateProductDTO } from './dto/create-product.dto';
import { FilterProductDTO } from './dto/filter-product.dto';
import { UpdateProductDTO } from './dto/update-product.dto';
import { Product } from './interface/product.interface';
import { ProductResponseHelper } from './product-response-helper';
import { ProductService } from './product.service';

@ApiTags('Product')
@Controller('product')
export class ProductController {

  constructor(
    private readonly _productService: ProductService,
    private readonly _responseHelper: ProductResponseHelper
  ) {}


  @Get()
  async findAll(): Promise<Product[]> {
    return this._productService.findAll();
  }


  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin, Role.User)
  @Get()
  async filter(@GetCurrentUserById() userId: string, @Query() query: FilterProductDTO): Promise<Product[]> {
    const products = await this._productService.filter(query);
    products.forEach(async product => product.details = await this._responseHelper.prepareDetailsObject(product.details, userId));
    return products;
  }


  @ApiParam({ name: 'id'})
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin, Role.User)
  @Get(':id')
  async findById(@GetCurrentUserById() userId: string, @Param('id') id: string): Promise<Product> {
    const product = await this._productService.findById(id);
    product.details = await this._responseHelper.prepareDetailsObject(product.details, userId);
    return product;
  }

  
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @Post()
  async create(@GetCurrentUserById() userId: string, @Body() dto: CreateProductDTO): Promise<Product> {
    const product = await this._productService.create(dto);
    product.details = await this._responseHelper.prepareDetailsObject(product.details, userId);
    return product;
  }


  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @Patch()
  async updateById(@GetCurrentUserById() userId: string, @Body() dto: UpdateProductDTO): Promise<Product> {
    const product = await this._productService.update(dto);
    product.details = await this._responseHelper.prepareDetailsObject(product.details, userId);
    return product;
  }


  @ApiParam({ name: 'id'})
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @Delete(':id')
  async deleteById(@GetCurrentUserById() userId: string, @Param('id') id: string): Promise<Product> {
    const product = await this._productService.deleteById(id);
    product.details = await this._responseHelper.prepareDetailsObject(product.details, userId);
    return product;
  }

}

