import * as mongoose from "mongoose";
import { SchemaName } from '../../utils/constants';

export const ProductSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      trim: true,
      required: true
    },

    image_url: {
      type: String,
      trim: true,
    },

    price: {
      type: Number,
      min: 0,
      required: true
    },

    details: {
      type: mongoose.Schema.Types.Mixed
    }
  },

  { 
    timestamps: true,
    collection: SchemaName.Product,
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
      },
    },
  },
);

ProductSchema.index({price: 1});