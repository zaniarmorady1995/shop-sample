import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { SchemaName } from 'src/utils/constants';
import { BaseRepository } from 'src/general/base.repository';
import { Product } from '../interface/product.interface';
import { UpdateProductDTO } from '../dto/update-product.dto';
import { FilterProductDTO } from '../dto/filter-product.dto';

@Injectable()
export class ProductRepository extends BaseRepository<Product> {

  constructor(
    @InjectModel(SchemaName.Product)
    private readonly _productModel: Model<Product>
  ) {
    super(_productModel);
  }


  async filter(options: FilterProductDTO): Promise<Product[]> {
    let query = {
      price: { $gte: options.price.min, $lte: options.price.max }
    };

    Object.entries(options.details).forEach(([key,value]) => {
      query[`details.${key}`] = value;
    });

    return this._productModel.find(query);
  }


  async update(object: UpdateProductDTO): Promise<Product> {
    let updateObject = {
      name: object.name,
      image_url: object.image_url,
      price: object.price,
    };

    Object.entries(object.details).forEach(([key,value]) => {
      updateObject[`details.${key}`] = value;
    });

    return this._productModel.findByIdAndUpdate(object.id, updateObject, {new: true});
  }

}