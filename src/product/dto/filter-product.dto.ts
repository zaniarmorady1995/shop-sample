import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsOptional } from "class-validator";

export class Price {
  @ApiPropertyOptional()
  @IsOptional()
  min: number;

  @ApiPropertyOptional()
  @IsOptional()
  max: number;
}


export class FilterProductDTO {
  @ApiPropertyOptional()
  @IsOptional()
  price: Price;

  @ApiPropertyOptional()
  @IsOptional()
  details: object = {};
}