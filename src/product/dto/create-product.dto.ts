import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsNotEmpty, IsOptional, Min } from "class-validator";

export class CreateProductDTO {
  @ApiProperty()
  @IsNotEmpty()
  name: string;

  @ApiPropertyOptional()
  @IsOptional()
  image_url: string;

  @ApiProperty()
  @IsNotEmpty()
  @Min(0)
  price: number;

  @ApiProperty({description: 'Contain data like color, size, ...'})
  @IsNotEmpty()
  details: object;
}