import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsMongoId, IsNotEmpty, IsOptional, Min } from "class-validator";

export class UpdateProductDTO {
  @ApiProperty({description: 'Id of product that you want to update it'})
  @IsNotEmpty()
  @IsMongoId({message: 'Wrong id'})
  id: string;

  @ApiPropertyOptional()
  @IsOptional()
  name: string;

  @ApiPropertyOptional()
  @IsOptional()
  image_url: string;

  @ApiPropertyOptional()
  @Min(0)
  @IsOptional()
  price: number;

  @ApiPropertyOptional({description: 'Contains data like color, size, ...'})
  @IsOptional()
  details: object;
} 