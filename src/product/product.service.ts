import { Injectable, NotFoundException } from '@nestjs/common';
import { Utilities } from 'src/utils/utilities';
import { ProductRepository } from './database/product.repository';
import { CreateProductDTO } from './dto/create-product.dto';
import { FilterProductDTO } from './dto/filter-product.dto';
import { UpdateProductDTO } from './dto/update-product.dto';
import { Product } from './interface/product.interface';

@Injectable()
export class ProductService {

  constructor(
    private readonly _productRepository: ProductRepository
  ){}


  async create(dto: CreateProductDTO): Promise<Product> {
    return this._productRepository.create(dto);
  }


  async findAll(): Promise<Product[]> {
    return this._productRepository.findAll();
  }


  async findById(id: string): Promise<Product> {
    const product = await this._productRepository.findById(id);
    if (!product) throw new NotFoundException('Product not found');
    return product;
  }


  async deleteById(id: string): Promise<Product> {
    const product = await this._productRepository.deleteById(id);
    if (!product) throw new NotFoundException('Product not found');
    return product;
  }


  async update(dto: UpdateProductDTO): Promise<Product> {
    const product = await this._productRepository.update(dto);
    if (!product) throw new NotFoundException('Product not found');
    return product;
  }


  async filter(filterOptions: FilterProductDTO): Promise<Product[]> {
    let details = {};
    Object.entries(filterOptions.details).forEach(([key, value]) => {
      details[key] = Utilities.isNumber(value) ? Number(value):value ;
    });

    filterOptions.details = details;
    return this._productRepository.filter(filterOptions);
  }

}
