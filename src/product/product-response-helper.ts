import { CACHE_MANAGER, Inject, Injectable } from "@nestjs/common";
import { LanguageService } from "src/language/language.service";
import { Cache } from "cache-manager";
import { UserService } from "src/user/user.service";

@Injectable()
export class ProductResponseHelper {

  constructor(
    private readonly _languageService: LanguageService,
    private readonly _userService: UserService,

    @Inject(CACHE_MANAGER) 
    private readonly _cacheManager: Cache
  ) {}


  async prepareDetailsObject(details: object, userId: string) {
    let languageCode: string = await this._cacheManager.get(`${process.env.REDIS_KEY}:languageCode:${userId}`);

    if (!languageCode) {
      const user = await this._userService.findById(userId);
      languageCode = user.languageCode
    }

    const keys = Object.keys(details)
    const translated: object = await this._languageService.prepareKeys(languageCode, keys);
    
    let response = {};
    Object.entries(details).forEach(([key, value]) => {
      response[translated[key]] = value;
    });

    return response;
  }

}