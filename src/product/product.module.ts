import { Module } from '@nestjs/common';
import { ProductService } from './product.service';
import { ProductController } from './product.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { SchemaName } from '../utils/constants';
import { ProductSchema } from './database/product.schema';
import { ProductRepository } from './database/product.repository';
import { LanguageModule } from 'src/language/language.module';
import { ProductResponseHelper } from './product-response-helper';
import { UserModule } from 'src/user/user.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: SchemaName.Product, schema: ProductSchema }]),
    LanguageModule,
    UserModule
  ],
  controllers: [ProductController],
  providers: [
    ProductService,
    ProductRepository,
    ProductResponseHelper
  ]
})
export class ProductModule {}
