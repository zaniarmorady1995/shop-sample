
export class Utilities {

  static isNumber(str: string): boolean {
    return /^\d+$/.test(str);
  }

}