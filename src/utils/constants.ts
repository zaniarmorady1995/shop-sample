export enum SchemaName {
  Product = 'product',
  User = 'user',
  Language = 'language',
}
