import { Body, Controller, Patch, Post, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Roles } from 'src/general/role.decorator';
import { Role } from 'src/general/role.enum';
import { JwtAuthGuard } from 'src/guard/jwt.guard';
import { RolesGuard } from 'src/guard/role.guard';
import { UpdateUserDTO } from './dto/update-user.dto';
import { User } from './interface/user.interface';
import { UserService } from './user.service';

@ApiTags('User')
@Controller('user')
export class UserController {

  constructor(private readonly _userService: UserService) {}

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin, Role.User)
  @Patch()
  async update(@Body() dto: UpdateUserDTO): Promise<User> {
    return this._userService.update(dto);
  }

}
