import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { UserRepository } from './database/user.repository';
import { MongooseModule } from '@nestjs/mongoose';
import { SchemaName } from 'src/utils/constants';
import { UserSchema } from './database/user.schema';

@Module({
  imports: [MongooseModule.forFeature([{name: SchemaName.User, schema: UserSchema}])],
  controllers: [UserController],
  providers: [UserService, UserRepository],
  exports: [UserService]
})
export class UserModule {}
