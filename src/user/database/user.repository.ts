import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { SchemaName } from "src/utils/constants";
import { Model } from 'mongoose';
import { BaseRepository } from "src/general/base.repository";
import { User } from "../interface/user.interface";
import { UpdateUserDTO } from "../dto/update-user.dto";

@Injectable()
export class UserRepository extends BaseRepository<User> {

  constructor(
    @InjectModel(SchemaName.User)
    private readonly _userModel: Model<User>
  ) {
    super(_userModel);
  }


  async findByEmail(email: string): Promise<User> {
    return this._userModel.findOne({ email });
  }


  async update(updateObject: Partial<UpdateUserDTO>): Promise<User> {
    return this._userModel.findByIdAndUpdate(
      updateObject.id,
      { 
        languageCode: updateObject.languageCode,
        name: updateObject.name,
        avatar: updateObject.avatar,
        password: updateObject.password,
      },
      { new: true }
    );
  }

}