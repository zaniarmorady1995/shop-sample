import * as mongoose from "mongoose";
import { SchemaName } from '../../utils/constants';

export const UserSchema = new mongoose.Schema(
  {
    languageCode: { // en, fa
      type: String,
      trim: true,
      required: true
    },

    name: {
      type: String,
      trim: true,
      required: true
    },

    email: {
      type: String,
      unique: true,
      trim: true,
      required: true
    },

    avatar: {
      type: String,
      required: false
    },

    password: {
      type: String,
      required: true
    },

    role: {
      type: String,
      enum : ['user','admin'],
      default: 'user'
    }

  },

  { 
    timestamps: true,
    collection: SchemaName.User,
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
      },
    },
  },
)

UserSchema.index({email: 1});