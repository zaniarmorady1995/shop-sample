import { BadRequestException, Injectable, NotFoundException, CACHE_MANAGER, Inject } from '@nestjs/common';
import { SignupDTO } from 'src/general/signup.dto';
import { UserRepository } from './database/user.repository';
import { UpdateUserDTO } from './dto/update-user.dto';
import { User } from './interface/user.interface';
import { Cache } from "cache-manager";

@Injectable()
export class UserService {

  constructor(
    private readonly _userRepository: UserRepository,

    @Inject(CACHE_MANAGER)
    private readonly _cacheManager: Cache
  ) {}


  async update(dto: Partial<UpdateUserDTO>): Promise<User> {
    const user = await this._userRepository.update(dto);
    if (!user) throw new NotFoundException('User not found');
    await this._cacheManager.set(`${process.env.REDIS_KEY}:languageCode:${user.id}`, user.languageCode);
    return user;
  }


  async findByEmail(email: string): Promise<User> {
    const user = await this._userRepository.findByEmail(email);
    if (!user) throw new NotFoundException('User not found');
    return user;
  }


  async findById(id: string): Promise<User> {
    const user = await this._userRepository.findById(id);
    if (!user) throw new NotFoundException('User not found');
    return user;
  }


  async create(dto: SignupDTO): Promise<User> {
    const user = await this._userRepository.findByEmail(dto.email);
    if (user) throw new BadRequestException('User already exists');

    const newUser: Partial<User> = {
      name: dto.name,
      email: dto.email,
      password: dto.password,
      avatar: dto.avatar,
      languageCode: dto.languageCode
    }

    const savedUser = await this._userRepository.create(newUser);
    await this._cacheManager.set(`${process.env.REDIS_KEY}:languageCode:${savedUser.id}`, savedUser.languageCode);
    return savedUser;
  }

}
