import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsEmpty, IsMongoId, IsNotEmpty, IsOptional, IsString, Length, Matches, MaxLength, MinLength } from "class-validator";

export class UpdateUserDTO {
  @IsNotEmpty()
  @ApiProperty()
  @IsMongoId({message: 'Wrong id'})
  id: string;

  @ApiPropertyOptional()
  @IsOptional()
  languageCode: string;

  @ApiPropertyOptional()
  @IsOptional()
  name: string;

  @ApiPropertyOptional()
  @IsOptional()
  avatar: string;

  @IsEmpty()
  password: string;
}