import { ApiProperty } from "@nestjs/swagger";
import { IsMongoId, IsNotEmpty, IsString, Length, Matches } from "class-validator";
import { SigninDTO } from "./signin.dto";

export class ChangePasswordDTO extends SigninDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsMongoId({message: 'Wrong id'})
  id: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @Length(8, 24)
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, { message: 'password too weak' })
  newPassword: string;
}