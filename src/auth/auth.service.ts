import { BadRequestException, Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from 'src/user/user.service';
import { SigninDTO } from './dto/signin.dto';
import { SignupDTO } from '../general/signup.dto';
import * as bcrypt from 'bcrypt';
import { ChangePasswordDTO } from './dto/change-password.dto';

@Injectable()
export class AuthService {

  constructor(
    private readonly _jwtService: JwtService,
    private readonly _userService: UserService
  ) {}


  async changePassword(dto: ChangePasswordDTO) {
    const user = await this._userService.findByEmail(dto.email);
    const isPasswordMatching = await bcrypt.compare(dto.password, user.password);
    if (!isPasswordMatching) throw new BadRequestException('Wrong data');

    const salt = await bcrypt.genSalt();
    const hashedPassword = await bcrypt.hash(dto.newPassword, salt);
    return this._userService.update({password: hashedPassword, id: dto.id});
  }


  async signup(dto: SignupDTO): Promise<string> {
    try {
      await this._userService.findByEmail(dto.email);
      throw new BadRequestException('User already exists');

    } catch (_) {
      const salt = await bcrypt.genSalt();
      const hashedPassword = await bcrypt.hash(dto.password, salt);
      dto.password = hashedPassword;
      const newUser = await this._userService.create(dto);
      return this.sign(newUser.id, newUser.role);
    }
  }


  async signin(dto: SigninDTO): Promise<string> {
    try {
      const user = await this._userService.findByEmail(dto.email);
      const isPasswordMatching = await bcrypt.compare(dto.password, user.password);
      if (isPasswordMatching) return this.sign(user.id, user.role);
      
      throw new UnauthorizedException('User does not exist');
    } catch (error) {
      throw new UnauthorizedException('User does not exist');
    }
  }


  sign(id: string, role: string): string {
    return this._jwtService.sign({sub: id, role});
  }

}
