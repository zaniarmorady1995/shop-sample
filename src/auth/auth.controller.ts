import { Body, Controller, Patch, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { SigninDTO } from './dto/signin.dto';
import { SignupDTO } from '../general/signup.dto';
import { ApiTags } from '@nestjs/swagger';
import { ChangePasswordDTO } from './dto/change-password.dto';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {

  constructor(private readonly _authService: AuthService) {}
  
  @Post('signin')
  async signin(@Body() dto: SigninDTO) {
    return { token: await this._authService.signin(dto) };
  }


  @Post('signup')
  async signup(@Body() dto: SignupDTO) {
    return { token: await this._authService.signup(dto) };
  }


  @Patch() 
  async changePassword(@Body() dto: ChangePasswordDTO) {
    await this._authService.changePassword(dto);
    return 'Password successfully updated';
  }

}
