import { CacheModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProductModule } from './product/product.module';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { LanguageModule } from './language/language.module';
import * as redisStore from "cache-manager-redis-store";
import 'dotenv/config';

@Module({
  imports: [
    MongooseModule.forRoot(
      `mongodb://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}:${process.env.DB_PORT}`, {
      dbName: process.env.DB_NAME,
    }),

    CacheModule.register({
      store: redisStore,
      host: process.env.REDIS_HOST,
      port: process.env.REDIS_PORT,
      name: process.env.REDIS_NAME,
      isGlobal: true
    }),
    
    ProductModule,
    UserModule,
    AuthModule,
    LanguageModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
